import cv2 as cv
import numpy as np
import argparse
from Functions import *


def main():
    #Reading image
    vImage = cv.imread(args["image"])

    #Variables for Gaussian flter, size of the kernel and value of the sigma
    vKernelSize = (5,5)
    vSigma = 5 
    
    #Coordinates for finding outer and inner line
    OuterCoords = [(0,550),(1050,700),(600,250)]
    InnerCoords = [(200,vImage.shape[0]),(500,vImage.shape[0]),(550,350),(500,350)]
    
    #Converting to Gray Scale value 
    VGreyImage = cv.cvtColor(vImage,cv.COLOR_RGB2GRAY)
    #Noise reduction by filter: Gaussian Blurring
    vGaussBlurr = cv.GaussianBlur(VGreyImage,vKernelSize,vSigma)

    vMaskedRegion = Mask(vGaussBlurr,OuterCoords,100,200)
    vMaskedRegion2 = Mask(vGaussBlurr,InnerCoords,10,50)

    vOuterLines = cv.HoughLinesP(vMaskedRegion,2,np.pi/180, 90,np.array([]),minLineLength=50,maxLineGap=5)
    vInnerLines = cv.HoughLinesP(vMaskedRegion2,2,np.pi/180, 90,np.array([]),minLineLength=20,maxLineGap=5)

    #cv.imshow("test1",vOuterLines)
    #cv.imshow("test2",vInnerLines)

    avergageLines = avergage_Lines(vImage,vOuterLines,True)
    avergageLines2 = avergage_Lines(vImage,vInnerLines,False)

    #Drawing rectangles:


    line_image = Display_lines(vImage,avergageLines,(0,255,0),10)
    line_image2 = Display_lines(vImage,avergageLines2,(0,0,255),5)

    #Drawing lines on our Image:
    vImage = cv.bitwise_or(line_image,vImage)
    vImage = cv.bitwise_or(line_image2,vImage)

    cv.imshow("Final Image",vImage)

    cv.waitKey(0)




if __name__ == '__main__':
    VargParse = argparse.ArgumentParser()
    VargParse.add_argument("-i","--image",required=True)
    args = vars(VargParse.parse_args())
    main()