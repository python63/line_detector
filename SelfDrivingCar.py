import cv2 as cv
import numpy as np
import argparse
from Functions import *


def main():    
    #Reading Image
    #vImage = cv.imread(args["image"])
    vCapture = cv.VideoCapture(args["video"])

    #Variables for Gaussian flter, size of the kernel and value of the sigma
    vKernelSize = (5,5)
    vSigma = 5  

    while vCapture.isOpened():
        _, vFrame = vCapture.read()

        #Coordinates for finding outer and inner line
        OuterCoords = [(0,550),(1050,700),(600,250)]
        InnerCoords = [(200,vFrame.shape[0]),(500,vFrame.shape[0]),(550,350),(500,350)]

        #Converting to Gray Scale value 
        VGreyImage = cv.cvtColor(vFrame,cv.COLOR_RGB2GRAY)
        #Noise reduction by filter: Gaussian Blurring
        vGaussBlurr = cv.GaussianBlur(VGreyImage,vKernelSize,vSigma)

        #Setting canny edge detector on specific area(where lines should be - depending on camera's setting)
        vMaskedRegion = Mask(vGaussBlurr,OuterCoords,100,200)
        vMaskedRegion2 = Mask(vGaussBlurr,InnerCoords,10,50)

        #Detecting lines on specific region
        vOuterLines = cv.HoughLinesP(vMaskedRegion,2,np.pi/180, 90,np.array([]),minLineLength=50,maxLineGap=5)
        vInnerLines = cv.HoughLinesP(vMaskedRegion2,2,np.pi/180, 90,np.array([]),minLineLength=20,maxLineGap=5)

        #Average lines, creatinng one smooth line
        averageLines = avergage_Lines(vFrame,vOuterLines,True)
        averageLines2 = avergage_Lines(vFrame,vInnerLines,False)

        #Calculating coordinates of the lines
        line_image = Display_lines(vFrame,averageLines,(0,255,0),10)
        line_image2 = Display_lines(vFrame,averageLines2,(0,0,255),5)

        #Drawing lines in single Frame
        vFrame = cv.bitwise_or(line_image,vFrame)
        vFrame = cv.bitwise_or(line_image2,vFrame)

        
        
        cv.imshow("Test",vFrame)
        #Quitting from Video
        if cv.waitKey(1) & 0xFF == ord('q'):
            break
    vCapture.release()
    cv.destroyAllWindows()

if __name__ == '__main__':
    VargParse = argparse.ArgumentParser()
    VargParse.add_argument("-i","--image",required=True)
    VargParse.add_argument("-v","--video",required=True)
    args = vars(VargParse.parse_args())
    main()
    